const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

const createRouter = (db) => {
	// Product index
	router.get('/', (req, res) => {
		db.query('SELECT * FROM `categories`', function (error, results) {
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				res.send(results);
			}
		});
	});
	
	// Product create
	router.post('/', upload.fields([{ category_name: '', maxCount: 1 },{ description: '', maxCount: 1 }]),
		(req, res) => {
		const category = req.body;
		
		db.query(
			'INSERT INTO `categories` (`category_name`, `description`) ' +
			'VALUES (?, ?)',
			[category.category_name, category.description],
			(error, results) => {
				if (error) {
					res.status(500);
					res.send(error);
				} else {
					category.id = results.insertId;
					res.send(category);
				}
			}
		);
	});
	
	// Product get by ID
	router.get('/:id', (req, res) => {
		db.query(`SELECT * FROM categories WHERE ID=${req.params.id}`, function (error, results) {
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				res.send(results);
			}
		});
	});
	
	router.delete('/:id', (req, res) => {
		db.query(`DELETE FROM categories WHERE ID=${req.params.id}`, function (error, results) {
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				res.send(results);
			}
		});
	});
	
	router.put('/:id', upload.fields([{ category_name: '', maxCount: 1 },{ description: '', maxCount: 1 }]),
		(req, res) => {
		const item = req.body;
		db.query(
			`UPDATE categories SET
			category_name = '${item.category_name}',
			description = '${item.description}'
			WHERE ID=${req.params.id}`,
			(error, results) => {
				if (error) {
					res.status(500);
					res.send(error);
				} else {
					res.send(item);
				}
			}
		)
	});
	
	return router;
};

module.exports = createRouter;