const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

const createRouter = (db) => {
	// Product index
	router.get('/', (req, res) => {
		db.query('SELECT * FROM `locations`', function (error, results) {
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				res.send(results);
			}
		});
	});
	
	// Product create
	router.post('/',upload.fields([{ location_name: '', maxCount: 1 },{ description: '', maxCount: 1 }]),
		(req, res) => {
		const location = req.body;
		db.query(
			'INSERT INTO `locations` (`location_name`, `description`) ' +
			'VALUES (?, ?)',
			[location.location_name, location.description],
			(error, results) => {
				if (error) {
					res.status(500);
					res.send(error);
				} else {
					location.id = results.insertId;
					res.send(location);
				}
			}
		);
	});
	
	// Product get by ID
	router.get('/:id', (req, res) => {
		db.query(`SELECT * FROM locations WHERE ID=${req.params.id}`, function (error, results) {
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				res.send(results);
			}
		});
	});
	
	router.delete('/:id', (req, res) => {
		db.query(`DELETE FROM locations WHERE ID=${req.params.id}`, function (error, results) {
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				res.send(results);
			}
		});
	});
	
	router.put('/:id', upload.fields([{ location_name: '', maxCount: 1 },{ description: '', maxCount: 1 }]),
		(req, res) => {
		const item = req.body;
		
		db.query(
			`UPDATE locations SET
			location_name = '${item.location_name}',
			description = '${item.description}'
			WHERE ID=${req.params.id}`,
			(error, results) => {
				if (error) {
					res.status(500);
					res.send(error);
				} else {
					res.send(item);
				}
			}
		)
	});
	
	return router;
};

module.exports = createRouter;