const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

const createRouter = (db) => {
	// Product index
	router.get('/', (req, res) => {
		db.query('SELECT * FROM `items`', function (error, results) {
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				res.send(results.map(item => {
					return {id: item.id, name: item.item_name}
				}));
			}
		});
	});
	
	router.get('/:id', (req, res) => {
		db.query(`SELECT * FROM items WHERE ID=${req.params.id}`, function (error, results) {
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				res.send(results);
			}
		});
	});
	
	// Product create
	router.post('/', upload.single('image'), (req, res) => {
		const item = req.body;
		if (req.file) {
			item.image = req.file.filename;
		} else {
			item.image = null;
		}
		
		db.query(
			'INSERT INTO items (item_name, category_id, location_id, description, item_image) ' +
			'VALUES (?, ?, ?, ?, ?)',
			[item.name, item.category, item.location, item.description, item.image],
			(error, results) => {
				if (error) {
					res.status(500);
					res.send(error);
				} else {
					item.id = results.insertId;
					res.send(item);
				}
			}
		);
	});
	
	router.delete('/:id', (req, res) => {
		db.query(`DELETE FROM items WHERE ID=${req.params.id}`, function (error, results) {
			if (error) {
				res.status(500);
				res.send(error);
			} else {
				res.send(results);
			}
		});
	});
	
	router.put('/:id', upload.single('image'), (req, res) => {
		const item = req.body;
		if (req.file) {
			item.image = req.file.filename;
		} else {
			item.image = null;
		}
		
		db.query(
			`UPDATE items SET
			item_name = '${item.name}',
			category_id = '${item.category}',
			location_id = '${item.location}',
			description = '${item.description}',
			item_image = '${item.image}'
			WHERE ID=${req.params.id}`,
			(error, results) => {
				if (error) {
					res.status(500);
					res.send(error);
				} else {
					item.id = results.insertId;
					res.send(item);
				}
			}
		);
	});
	
	return router;
};

module.exports = createRouter;