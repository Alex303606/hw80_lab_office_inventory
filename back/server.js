const express = require('express');
const cors = require('cors');
const mysql = require('mysql');

const items = require('./app/items');
const categories = require('./app/categories');
const locations = require('./app/locations');
const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'root',
	password : 'root',
	database : 'office_inventory'
});

connection.connect((err) => {
	if (err) throw err;
	
	app.use('/items', items(connection));
	app.use('/categories', categories(connection));
	app.use('/locations', locations(connection));
	
	app.listen(port, () => {
		console.log(`Server started on ${port} port!`);
	});
});