import React from 'react';
import './item.css';

const Item = props => {
	return (
		<div className="item">
			<h2>Информация о предмете</h2>
			<span> <b>Название:</b> {props.item.item_name}</span>
			<span><b>Категория:</b> {props.item.category_id}</span>
			<span><b>Место положение:</b> {props.item.location_id}</span>
			<span><b>Описание:</b> {props.item.description}</span>
			{
				(props.item.item_image && props.item.item_image !== 'null') && <img src={'http://localhost:8000/uploads/' + props.item.item_image} alt=""/>
			}
		</div>
	)
};

export default Item;