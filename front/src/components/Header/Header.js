import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css';

const Header = () => {
	return (
		<header>
			<NavLink exact to="/">Список предметов</NavLink>
			<NavLink exact to="/add_edit">Добавить предмет</NavLink>
		</header>
	)
};

export default Header;