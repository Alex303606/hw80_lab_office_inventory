import React, {Component, Fragment} from 'react';
import Header from "./components/Header/Header";
import Items from "./containers/Items/Items";
import AddEdit from "./containers/AddEdit/AddEdit";
import {Route, Switch} from "react-router-dom";

class App extends Component {
	render() {
		return (
			<Fragment>
				<Header/>
				<Switch>
					<Route exact path="/" component={Items}/>
					<Route exact path="/add_edit" component={AddEdit}/>
				</Switch>
			</Fragment>
		);
	}
}

export default App;
