import {FETCH_GET_SUCCESS, FETCH_POST_SUCCESS, FETCH_POST_ERROR, FETCH_DELETE_SUCCESS} from "./actionTypes";

const initialState = {
	items: [],
	error: ''
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_POST_SUCCESS:
			const items = [...state.items];
			items.push(action.data);
			return {...state, items, error: ''};
		case FETCH_POST_ERROR:
			return {...state, error: action.error.response.data.sqlMessage};
		case FETCH_GET_SUCCESS:
			return {...state, items: action.items, error: ''};
		case FETCH_DELETE_SUCCESS:
			const NewItems = [...state.items];
			const index = NewItems.findIndex(p => p.id === action.id);
			NewItems.splice(index,1);
			return {...state, items: NewItems, error: ''};
		default:
			return state;
	}
};

export default reducer;
