import {FETCH_GET_SUCCESS, FETCH_POST_SUCCESS, FETCH_POST_ERROR, FETCH_DELETE_SUCCESS} from "./actionTypes";
import axios from '../axios-api';

export const fetchPostSuccess = data => {
	return {type: FETCH_POST_SUCCESS, data};
};

export const fetchGetSuccess = items => {
	return {type: FETCH_GET_SUCCESS, items};
};

export const fetchPostError = error => {
	return {type: FETCH_POST_ERROR, error}
};

export const fetchDeleteSuccess = id => {
	return {type: FETCH_DELETE_SUCCESS, id}
};

export const postFormToServerDb = data => {
	return dispatch => {
		return axios.post('/items', data).then(
			response => dispatch(fetchPostSuccess(response.data))
		).catch(error => dispatch(fetchPostError(error)));
	}
};

export const getItems = () => {
	return dispatch => {
		return axios.get('/items').then(
			response => dispatch(fetchGetSuccess(response.data))
		);
	};
};

export const getItemByID = id => {
	return () => {
		return axios.get(`/items/${id}`).then(response => response.data);
	}
};

export const deleteItem = id => {
	return dispatch => {
		return axios.delete(`/items/${id}`).then(
			response => dispatch(fetchDeleteSuccess(id))
		).catch(error => dispatch(fetchPostError(error)))
	}
};
