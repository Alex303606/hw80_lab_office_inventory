import React, {Component, Fragment} from 'react';
import './Items.css';
import {connect} from "react-redux";
import {deleteItem, getItemByID, getItems} from "../../store/actions";
import Modal from "../../components/UI/Modal/Modal";
import Item from "../../components/Item/Item";
import Spinner from "../../components/UI/Spinner/Spinner";

class Items extends Component {
	
	state = {
		purchasing: false,
		loading: true,
		item: {
			category_id: null,
			description: '',
			id: null,
			item_image: '',
			item_name: '',
			location_id: null
		}
	};
	
	componentDidMount() {
		this.props.getAllItems();
	};
	
	purchaseCancelHandler = () => {
		this.setState({purchasing: false})
	};
	
	getItem = id => {
		this.setState({purchasing: true});
		this.props.getItemByID(id).then(response => {
			this.setState({item: response[0]});
		}).then(() => this.setState({loading: false}));
	};
	
	deleteItem = id => {
		this.props.deleteItemFromDB(id);
	};
	
	render() {
		return (
			<Fragment>
				<div className="items">
					<ul>
						{
							this.props.items.map(item => {
								return <li
									key={item.id}
									className="this-item">
									<b onClick={() => this.getItem(item.id)}>{item.name}</b>
									<button className="delete" onClick={() => this.deleteItem(item.id)}>Удалить</button>
									<button className="edit">Редактировать</button>
									</li>
							})
						}
					</ul>
				</div>
				<Modal show={this.state.purchasing}
				       closed={this.purchaseCancelHandler}>
					{
						this.state.loading ? <Spinner/> : <Item item={this.state.item}/>
					}
				</Modal>
			</Fragment>
		
		)
	}
}

const mapStateToProps = state => {
	return {
		items: state.items,
		error: state.error
	};
};

const mapDispatchToProps = dispatch => {
	return {
		getAllItems: () => dispatch(getItems()),
		getItemByID: (id) => dispatch(getItemByID(id)),
		deleteItemFromDB: (id) => dispatch(deleteItem(id))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Items);